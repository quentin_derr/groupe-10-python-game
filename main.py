import pygame

from models.MainScreen import MainScreen
from models.PresidentGame import PresidentGame

RUN = True
START_GAME = False

pygame.init()
main_screen = MainScreen()

while RUN:
    # quit 💔
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            RUN = False

    # home screen
    if not START_GAME:
        main_screen.screen.fill((202, 228, 241))
        logo_rect = main_screen.logo.get_rect(center=(main_screen.SCREEN_WIDTH / 2, main_screen.SCREEN_HEIGHT * 0.25))
        main_screen.screen.blit(main_screen.logo, logo_rect)

        if main_screen.start_button.draw(main_screen.screen):
            START_GAME = True

        if main_screen.exit_button.draw(main_screen.screen):
            RUN = False

    # ♦️♣️♠️♥️ GAME IS BEGINNING ♦️♣️♠️♥️
    else:
        font = pygame.font.SysFont('arial', 30)
        text = font.render("!! Waiting for terminal input !!", True, (0, 0, 0))
        main_screen.screen.blit(text, (MainScreen.SCREEN_WIDTH // 2 - text.get_width() // 2, 20))
        pygame.display.update()

        game = PresidentGame()
        game.start()

        game_over = False
        while not game_over:

            for current_player in game.players:
                main_screen.screen.fill('#8792AE')  # Fill the screen to reset the display

                card_button = current_player.show_cards(main_screen.screen)  # Display the cards of the current player
                game.display_last_card(main_screen.screen)  # Show last card played

                game.show_name_and_nb_cards(main_screen.screen, current_player)  # Show main infos of the game
                pygame.display.update()

                playable_cards = []

                for card in current_player.hand:
                    if len(game.board) == 0 or card.names[card.name] >= game.board[-1].names[game.board[-1].name]:
                        playable_cards.append(card)

                player_played = game.check_plays(playable_cards)
                while not player_played:
                    # By pressing spacebar, the player can skip his turn
                    if pygame.key.get_pressed()[pygame.K_SPACE]:
                        player_played = True
                        pygame.time.wait(500)

                    for event in pygame.event.get():
                        if event.type == pygame.MOUSEBUTTONDOWN:
                            for card in card_button:

                                if card.draw(main_screen.screen):
                                    # Check if the card is in the list of playable cards
                                    if card.card in playable_cards:
                                        # Add the card to the board and remove it from the player's hand
                                        nb = 1
                                        game.board.append(card.card)
                                        current_player.hand.remove(card.card)

                                        while card.card.name in [c.name for c in current_player.hand]:
                                            nb += 1
                                            game.nb_to_play = nb
                                            game.board.append(card.card)
                                            current_player.hand.remove(card.card)

                                        print("Playing {} {}".format(nb, card.card))
                                        player_played = True
                                    else:
                                        font = pygame.font.SysFont('arial', 30)
                                        text = font.render("You can't play this card, {} !".format(current_player.name), True, "#DD4949")  # Hey you can't play this card ! 😠
                                        main_screen.screen.blit(text, (
                                            main_screen.SCREEN_WIDTH // 2 + 110 - text.get_width() // 2, 500))
                                        pygame.display.update()

                            if not current_player.hand:
                                current_player.name += " (PRESIDENT)"
                                break

    pygame.display.update()

# quit Pygame 💔
pygame.quit()
