## ♦️♣️♠️♥️ Le jeu du président ♥️♠️♣️♦️️

* [General info](#general-info)
* [Technologies](#technologies)
* [How does it work ?](#how-does-it-work)
* [Setup](#setup)
* [Not-working / Bugs](#not-working--bugs)


# 1. General info
- This game has been developed for the "Développement en Python" course.
- It is a card game, that can be played by 3 to 6 players.
- The goal is to get rid of all your cards before the other players to be declared the "Président".

# 2. How does it work ?
![schema.png](schema.png)

# 3. Technologies :
#### Python 3.9
#### Pygame 2.0.1

# 4. Setup :
```
sh install.sh
```

# 5. Not-working / Bugs :
- The game is not finished yet, so there are a lot of bugs and not-working features.
- I don't know why, but you can't click twice on the same spot (??)
- The game doesn't take in minds the number of cards in each turn (can be 1, 2, 3 or 4)