import pygame

import models.Buttons as button


class MainScreen:
    SCREEN_WIDTH = 1000
    SCREEN_HEIGHT = 800

    def __init__(self):
        # Home screen
        self.screen = pygame.display.set_mode((self.SCREEN_WIDTH, self.SCREEN_HEIGHT))
        pygame.display.set_caption('Le jeu du président')

        # Images
        self.start_img = pygame.image.load('./assets/play.png').convert_alpha()
        self.exit_img = pygame.image.load('./assets/quit.png').convert_alpha()
        self.logo = pygame.image.load('./assets/logo-president.png').convert_alpha()

        # Buttons
        self.start_button = button.Button(self.SCREEN_WIDTH // 2 - 120, self.SCREEN_HEIGHT // 2, self.start_img, 0.8)
        self.exit_button = button.Button(self.SCREEN_WIDTH // 2 - 120, self.SCREEN_HEIGHT // 2 + 200, self.exit_img,
                                         0.8)

    def draw(self):
        # Game logo
        logo_rect = self.logo.get_rect()
        logo_rect.center = (self.SCREEN_WIDTH // 2, self.SCREEN_HEIGHT // 2 - 300)
        self.screen.blit(self.logo, logo_rect)

        # Main screen's button
        self.start_button.draw(self.screen)
        self.exit_button.draw(self.screen)
