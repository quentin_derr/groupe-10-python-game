class Card:
    names = {'3': 1, '4': 2, '5': 3, '6': 4, '7': 5, '8': 6, '9': 7, '10': 8, 'J': 9, 'Q': 10, 'K': 11, 'A': 12, '2': 13}
    suits = ["D", "H", "S", "C"] # Diamonds, Hearts, Spades, Clubs

    def __init__(self, name=None, suit=None, image=None):
        self.suit = suit
        self.name = name
        self.image = name + suit + ".png"

    def __str__(self):
        return self.name + self.suit

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        return self.names[self.name] == self.names[other.name]

    def __lt__(self, other):
        return self.names[self.name] < self.names[other.name]

    def __gt__(self, other):
        return self.names[self.name] > self.names[other.name]

