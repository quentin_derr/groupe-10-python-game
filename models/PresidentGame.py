import random
import pygame

from models.Card import Card
from models.Deck import Deck
from models.Player import Player
from models.MainScreen import MainScreen


class PresidentGame:

    # 3 player by default
    def __init__(self, players=None):
        self.board = []
        self.players = [] or [Player(), Player(), Player()]
        self.deck = Deck()
        self.nb_to_play = 1

    def name_players(self):
        # Reset the array of default players
        self.players = []

        # Ask the number of players
        number_of_player = int(input("How many players ? (3-6)"))
        for i in range(number_of_player):
            # Players names must be unique
            while True:
                name = input("Enter the name of the player ({}): ".format(i + 1))
                if name not in [player.name for player in self.players]:
                    break

            self.players.append(Player(name))

    def start(self):
        self.name_players()
        self.deck.shuffle()
        self.distribute_cards()

    def distribute_cards(self):
        for player in self.players:
            player.hand = []

        for i, card in enumerate(self.deck.cards):
            self.players[i % len(self.players)].hand.append(card)

    def display_last_card(self, screen):
        if self.board:
            last_card = pygame.image.load('assets/cartes-png/{}'.format(self.board[-1].image)).convert_alpha()
            last_card = pygame.transform.scale(last_card, (140, 220))
            screen.blit(last_card, (MainScreen.SCREEN_WIDTH // 2 - 70, MainScreen.SCREEN_HEIGHT // 2 - 250))

    def show_name_and_nb_cards(self, screen, current_player):
        """
        Show the name of the current player and the number of cards he has
        :param screen:
        :param current_player:
        :return:
        """
        # Display the current player name
        font = pygame.font.SysFont('arial', 30)
        text = font.render("{}'s turn".format(current_player.name), True, (0, 0, 0))
        screen.blit(text, (MainScreen.SCREEN_WIDTH // 2 - text.get_width() // 2, 20))

        # A player can skip !
        text = font.render("You can skip your turn by pressing spacebar", True, (0, 0, 0))
        screen.blit(text, (MainScreen.SCREEN_WIDTH // 2 - text.get_width() // 2, 50))

        # Show the number of cards of each player
        for i, player in enumerate(self.players):
            font = pygame.font.SysFont('arial', 20)
            text = font.render("Card(s) of {} : {}".format(player.name, len(player.hand)), True, (0, 0, 0))
            screen.blit(text, (50, MainScreen.SCREEN_HEIGHT - 430 + i * 30))

    def check_plays(self, playable_cards):
        """
        Check if the player can play a card
        :param playable_cards
        :return: bool
        """
        print("playable_cards", playable_cards)

        if playable_cards:
            return False
        else:
            self.board = []
            return True
