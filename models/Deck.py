from models.Card import Card
from random import shuffle


class Deck:
    def __init__(self):
        self.cards = []
        for suit in Card.suits:
            for name in Card.names:
                self.cards.append(Card(name, suit))

    def __repr__(self):
        return str(self.cards)

    def shuffle(self):
        shuffle(self.cards)
