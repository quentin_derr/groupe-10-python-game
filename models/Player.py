import random

import pygame
import models.Buttons as button


class Player:

    def __init__(self, name=None):
        self.name = name or 'Joueurs {}'.format(random.randint(1, 1000))
        self.hand = []
        self.rank = "Joueur"
        self.lastPlayed = None

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    def __eq__(self, other):
        return self.name == other.name

    def remove_from_hand(self, card):
        self.hand.remove(card)

    def show_cards(self, screen):
        card_button_array = []

        # Create a button for each card in the player's hand
        for i in range(len(self.hand)):
            self.hand.sort(key=lambda x: x.names[x.name])
            card_image = pygame.image.load('assets/cartes-png/{}'.format(self.hand[i].image)).convert_alpha()
            card_image = pygame.transform.scale(card_image, (70, 110))

            if len(self.hand) > 11:
                card_button_array.append(button.Button(50 + 80 * (i % 11), 550 + 120 * (i // 11), card_image, 1, self.hand[i]))

            else:
                card_button_array.append(button.Button(50 + 80 * i, 550, card_image, 1, self.hand[i]))

        # Draw the buttons on screen
        for card in card_button_array:
            card.draw(screen)

        return card_button_array
