import unittest
from models.Card import Card
from models.Deck import Deck


class TestCardsExercice1(unittest.TestCase):
    def test_card_constructor(self):
        self.assertTrue(isinstance(Card('A', 'H'), Card))

    def test_cards_equal_value(self):
        ace_of_hearts = Card('A', 'H')
        ace_of_spades = Card('A', 'S')
        self.assertEqual(ace_of_hearts, ace_of_spades, 'Two cards having '
                                                       'same value should be considered equal')

    def test_cards_comparison(self):
        ace_of_hearts = Card('A', 'H')
        two_of_hearts = Card('2', 'H')
        five_of_hearts = Card('5', 'H')

        print(ace_of_hearts.name)

        self.assertTrue(ace_of_hearts > five_of_hearts)
        self.assertTrue(two_of_hearts > ace_of_hearts > five_of_hearts,
                        'The two card is the highest card')
        self.assertTrue(five_of_hearts < two_of_hearts,
                        'The two card is the highest card')


class TestDeckExercice1(unittest.TestCase):
    def test_deck_has_52_cards(self):
        deck = Deck()
        self.assertEqual(len(deck.cards), 52, 'The president is a card game '
                                              'requiring 52 cards')

    def test_deck_shuffling(self):
        deck_1 = Deck()
        deck_2 = Deck()
        self.assertEqual(deck_1.cards, deck_2.cards, 'A new deck should not be automatically shuffled')
        deck_2.shuffle()
        self.assertNotEqual(deck_1.cards, deck_2.cards, 'Shuffling a deck '
                                                        'randomizes the '
                                                        'cards order')


if __name__ == '__main__':
    unittest.main()
